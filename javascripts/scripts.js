//$.noConflict();
jQuery(document).ready(function($) {
	jQuery('audio,video').mediaelementplayer({
		startVolume: 0.8,
	});
});

jQuery(document).ready(function($){/* activate scrollspy menu */

	$('body').scrollspy({
	  target: '#navbar-collapsible',
	  offset: 55
	});
	

	
	/* smooth scrolling sections */
	$('a[href*=#]:not([href=#])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html,body').animate({
	          scrollTop: target.offset().top - 50
	        }, 2000);
	        return false;
	      }
	    }
	});

});