<?php
	$config = array(
		'project-title' => "Dave Brown",
		'page-title' => "Dave Brown",
		'name' => "Dave Brown",
		'subhead' => "Presenter",
		'demos' => array(
			'radio' => array(
				'title' => 'Radio Demo',
				'url' => 'media/audio/radio.mp3',
			),
				
			'voiceover' => array(
				'title' => "Voiceover Demo",
				'url' => 'media/audio/voiceover.mp3',
			)
		),
		'image_path' => "media/images/",
		'testminonials' => array(
			array(
				'quote' => "Dave was blah blah",
				'author' => "Ken Bruce",
				'image' => 'https://s3.amazonaws.com/uifaces/faces/twitter/kolage/128.jpg',
				'url' => 'http://www.bbc.co.uk/radio2',
			),
			array(
				'quote' => "Dave was blah blah",
				'author' => "Sarah Bright",
				'image' => 'https://s3.amazonaws.com/uifaces/faces/twitter/kolage/128.jpg',
				'url' => 'http://www.bbc.co.uk/radio2',
			),
		),
		'clients' => array(
			array(
				'image' => 'smoothradio.jpg',
			),
			array(
				'image' => 'broadland.png',
			),
			array(
				'image' => 'realradio.jpg',
			),
			array(
				'image' => 'bbcnorfolk.jpg',
			),
			array(
				'image' => 'bbcsuffolk.jpg',
			),
			array(
				'image' => 'jazzfm.jpg',
			)
		),
			/*<li class="col-sm-4 "><img src="media/images/broadland.png" class="img-responsive img-rounded" /></li>
        		<li class="col-sm-4"><img src="media/images/RR_FULL COLOUR_Generic Logo" class="img-responsive img-rounded" /></li>
        		<li class="col-sm-4"><img src="media/images/bbcnorfolk.jpg" class="img-responsive img-rounded" /></li>
        		<li class="col-sm-4"><img src="media/images/bbcsuffolk.jpg" class="text-center img-responsive img-rounded" /></li>
        		<li class="col-sm-4"><img src="media/images/Jazz-FM-LOGO-large-Copy.jpg" class="img-responsive img-rounded" /></li>

*/
		'socials' => array(
			array(
				'icon' => 'facebook',
				'url' => 'https://www.facebook.com/dave.brown.5076?fref=ts'
			),
			array(
				'icon' => 'twitter',
				'url' => 'http://www.twitter.com/davebsmooth'
			)
		)
	);
	function clients() {
		global $config;
		return $config['clients'];
	}
	function config($key) {
		global $config;
		return $config[$key];	
	}
	function socials() {
		global $config;
		return $config['socials'];
	}
	function get_testimonials() {
		global $config;
		return $config['testminonials'];
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title><?php echo $config['page-title']; ?></title>
		<meta name="generator" content="Fred Bradley | http://www.fredbradley.uk" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="stylesheets/styles.css" rel="stylesheet">
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

		<link href='http://fonts.googleapis.com/css?family=Kaushan+Script|Lato' rel='stylesheet' type='text/css'>
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
	<body>
<nav class="navbar navbar-trans navbar-fixed-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapsible">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#section1"><?php echo config('project-title'); ?></a>
    </div>
    <div class="navbar-collapse collapse" id="navbar-collapsible">
      <ul class="nav navbar-nav navbar-left">
        <li><a href="#section1">Home</a></li>
        <li><a href="#section2">About</a></li>
        <li><a href="#section3">Testimonials</a></li>
        <li><a href="#section4">Demos</a></li>
       <!-- <li><a href="#section5">Five</a></li> --> 
      <!--  <li><a href="#section6">Social Media</a></li> -->
        <li><a href="#section7">Contact Dave</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
	      <li><a href="http://www.twitter.com"><i class="fa fa-fw fa-twitter"></i></a></li>
      </ul>
    </div>
  </div>
</nav>

<section class="container-fluid" id="section1">
	



  	<h1 class="text-center v-center Kaushan"><?php echo $config['name']; ?></h1>
  	<h2 class="text-center Kaushan"><?php echo $config['subhead']; ?></h2>
 	
  	<div class="container">
  		<div class="row">
  			<div class="col-sm-6 col-sm-offset-3 text-center">
  				<img class="img-responsive img-rounded img-shadowed" src="media/images/dave.jpg" />
  			</div>
  		</div>
  		
      <div class="row biogs">
          <div class="col-sm-6">
            <div class="row">
              <div class="col-sm-10 col-sm-offset-2 text-center">
              	<h3>Presenter</h3>
              	<p>With over 30 years experience, Dave's warm style engages the audience while his expert knowledge of the music impresses everyone.</p>
              	<i class="shadow-bright fa fa-fw fa-bullhorn fa-5x"></i>
              </div>
            </div>
          </div>
          <div class="col-sm-6 text-center">
            <div class="row">
              <div class="col-sm-10 col-sm-offset-1 text-center">
              	<h3>Voiceover</h3>
              	<p>Adverts, imaging, continuity and corporate events. Dave has an authoritative style that gives off true confidence and instruction.</p>
              	<i class="shadow-bright fa fa-fw fa-microphone fa-5x"></i>
              </div>
            </div>
          </div>
      </div><!--/row-->
    <div class="row"><br></div>
  </div><!--/container-->
</section>

<section class="container-fluid" id="section2">
  <div class="row">
  	<div class="col-sm-8 col-sm-offset-2 text-center">
        <h1 class="Kaushan">About Dave</h1>
        <br>
		<p class="lead">Highly experienced presenter with over 30 years experience in both commercial and BBC radio. Experience at National and Local levels.</p>
		<p class="lead">Smooth UK ( Head of Presentation) Radio Broadland ( Programme Controller) BBC Suffolk and Norfolk , Real Radio Scotland and Wales and, Jazz FM</p>
        <br>
        <p class="text-center">
        	<ul class="list-unstyled clients row" >
        		<?php foreach (clients() as $client): ?>
        		<li class="col-sm-4 col-xs-6"><img src="<?php echo $config['image_path']; ?><?php echo $client['image']; ?>" class="text-center img-responsive img-rounded" /></li>
        		<?php endforeach; ?>
        	</ul>
        </p>
    </div>
  </div>
</section>

<section class="container-fluid" id="section3">
	<h1 class="text-center Kaushan">Things people say...</h1>
    <?php require_once('testimonials.php'); ?>
</section>

<section class="container-fluid" id="section4">
	<h2 class="text-center Kaushan">Hear Dave...</h2>
    <div class="row">
    	<?php 
    		foreach ($config['demos'] as $demokey => $demo): ?>
			<div class="col-sm-6 text-center">
				<h3><?php echo $demo['title']; ?></h3>
				<p>With over 30 years experience, Dave's warm style engages the audience while his expert knowledge of the music impresses everyone.</p>
				<i class="shadow-bright fa fa-fw fa-bullhorn fa-5x"></i>
				<div class="audio_container">
					<audio style="" id="<?php echo $demokey; ?>" src="<?php echo $demo['url']; ?>" type="audio/mp3" controls="controls"></audio>
				</div>
	
			</div>
      	<?php endforeach; ?>
    </div>
</section>
<? /*
<section class="container-fluid" id="section5">
  <div class="col-sm-10 col-sm-offset-1">
<!--    <div class="container"> -->
    <div class="row">
      <div class="col-sm-4 col-xs-12">
            <div class="list-group">
              <a href="#" class="list-group-item active">
                <h2 class="list-group-item-heading">Basic</h2>
                <h6>Free to get started</h6>
              </a>
              <a href="#" class="list-group-item">
                <p class="list-group-item-text">Option 100 - more about this</p>
              </a>
              <a href="#" class="list-group-item">
                <p class="list-group-item-text">Option 2 - this is more about this</p>
              </a>
              <a href="#" class="list-group-item">
                <p class="list-group-item-text">Option 3 GB</p>
              </a>
              <a href="#" class="list-group-item">
                <p class="list-group-item-text">Option 4</p>
              </a>
              <a href="#" class="list-group-item">
                <p class="list-group-item-text">Feature</p>
              </a>
              <a href="#" class="list-group-item">
                <p class="list-group-item-text">Feature</p>
              </a>
              <a href="#" class="list-group-item">
                <button class="btn btn-primary btn-lg btn-block">Get Started</button>
              </a>
            </div>
      </div><!--/left-->
      
      <div class="col-sm-4 col-xs-12">
            <div class="list-group text-center">
              <a href="#" class="list-group-item active">
                <h2 class="list-group-item-heading">Better</h2>
                <h6>Most popular plan</h6>
              </a>
              <a href="#" class="list-group-item">
                <p class="list-group-item-text">Option 200 - more about this</p>
              </a>
              <a href="#" class="list-group-item">
                <p class="list-group-item-text">Option 2 - this is more about this</p>
              </a>
              <a href="#" class="list-group-item">
                <p class="list-group-item-text">Option 5 GB</p>
              </a>
              <a href="#" class="list-group-item">
                <p class="list-group-item-text">Option 6</p>
              </a>
              <a href="#" class="list-group-item">
                <p class="list-group-item-text">Feature</p>
              </a>
              <a href="#" class="list-group-item">
                <p class="list-group-item-text">Feature</p>
              </a>
              <a href="#" class="list-group-item">
                <button class="btn btn-default btn-lg btn-block">$10 per month</button>
              </a>
            </div>
      </div><!--/middle-->
      
      <div class="col-sm-4 col-xs-12">
            <div class="list-group text-right">
              <a href="#" class="list-group-item active">
                <h2 class="list-group-item-heading">Best</h2>
                <h6>For enterprise grade</h6>
              </a>
              <a href="#" class="list-group-item">
                <p class="list-group-item-text">Option 100 - more about this</p>
              </a>
              <a href="#" class="list-group-item">
                <p class="list-group-item-text">Option 2 - this is more about this</p>
              </a>
              <a href="#" class="list-group-item">
                <p class="list-group-item-text">Option 8 GB</p>
              </a>
              <a href="#" class="list-group-item">
                <p class="list-group-item-text">Option 10</p>
              </a>
              <a href="#" class="list-group-item">
                <p class="list-group-item-text">Unlimited</p>
              </a>
              <a href="#" class="list-group-item">
                <p class="list-group-item-text">Unlimited</p>
              </a>
              <a href="#" class="list-group-item">
                <button class="btn btn-default btn-lg btn-block">$20 per month</button>
              </a>
            </div>
      </div><!--/right-->
      
    </div><!--/row-->
 <!--   </div><!--/container--> 
  </div>
</section>
*/ ?>
<section class="container-fluid" id="section6">
	<h1 class="text-center Kaushan">Social Media</h1>
    <div class="row">

		<div class="col-sm-12 text-center">
			<?php foreach (socials() as $social): ?>
				<a href="<?php echo $social['url']; ?>">
					<i class="fa fa-fw fa-<?php echo $social['icon']; ?> fa-4x"></i>
				</a>
			<?php endforeach; ?>
			<div class="row">
				<div class="col-sm-6 text-center">
					<a class="twitter-timeline" href="https://twitter.com/Davebsmooth" data-widget-id="535424519738241024">Tweets by @Davebsmooth</a>
				</div>
				<div class="col-sm-6 text-center"></div>
			</div>
		</div>
	
  </div><!--/row-->
</section>

<section class="container-fluid" id="section7">

  <h1 class="text-center Kaushan">Say Hello</h1>
  <div class="row">
  	<div class="col-sm-8 col-sm-offset-2 text-center">
  		<div class="row">
  			<div class="form-group col-sm-6">
  				<input class="form-control input-lg" name="your-name" placeholder="Your Name">
  			</div>
  			<div class="form-group col-sm-6">
  				<input class="form-control input-lg" name="your-email" placeholder="Your Email">
  			</div>
  			<div class="form-group col-sm-12">
  				<textarea class="form-control input-lg" name="message" placeholder="Message"></textarea>
  			</div>
  			<div class="form-group col-sm-12">
  				<button type="submit" class="btn btn-block btn-lg btn-success">Send Email</button>
  			</div>
  		</div>
      <p>
    	<a href="http://www.bootstrapzero.com/bootstrap-template/sectionalize">Get the code for this template.</a>
      </p>
    </div>
  </div>
</section>

<footer id="footer">
  <div class="container">
    <div class="row">    
      <div class="col-xs-6 col-sm-6 col-md-3 column">          
          <h4>Smooth Radio</h4>
          <ul class="nav">
            <li><a href="http://www.smoothradio.com/shows-presenters/through-the-night/">Through the Night</a></li>
            <li><a href="about-us.html">Services</a></li>
            <li><a href="about-us.html">Benefits</a></li>
            <li><a href="elements.html">Developers</a></li>
          </ul> 
        </div>
      <div class="col-xs-6 col-md-3 column">          
          <h4>Follow Us</h4>
          <ul class="nav">
            <li><a href="#">Twitter</a></li>
            <li><a href="#">Facebook</a></li>
            <li><a href="#">Google+</a></li>
            <li><a href="#">Pinterest</a></li>
          </ul> 
      </div>
      <div class="col-xs-6 col-md-3 column">          
          <h4>Contact Us</h4>
          <ul class="nav">
            <li><a href="#">Email</a></li>
            <li><a href="#">Headquarters</a></li>
            <li><a href="#">Management</a></li>
            <li><a href="#">Support</a></li>
          </ul> 
      </div>
      <div class="col-xs-6 col-md-3 column">          
          <h4>Customer Service</h4>
          <ul class="nav">
            <li><a href="#">About Us</a></li>
            <li><a href="#">Delivery Information</a></li>
            <li><a href="#">Privacy Policy</a></li>
            <li><a href="#">Terms &amp; Conditions</a></li>
          </ul> 
      </div>
    </div><!--/row-->
  </div>
</footer>

	<!-- script references -->
	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script> 
		<!-- <script src="javascripts/jquery.min.js"></script> -->
		<script src="javascripts/bootstrap.js"></script>
		<script src="javascripts/scripts.js"></script>
		<script src="javascripts/mediaelement-and-player.js"></script>
	</body>
</html>