<?php 
	$testimonials = array(
		array(
			'quote' => "Dave was blah blah",
			'author' => "Ken Bruce",
			'image' => 'https://s3.amazonaws.com/uifaces/faces/twitter/kolage/128.jpg',
			'url' => 'http://www.bbc.co.uk/radio2',
		),
		array(
			'quote' => "Dave was blah blah",
			'author' => "Sarah Bright",
			'image' => 'https://s3.amazonaws.com/uifaces/faces/twitter/kolage/128.jpg',
			'url' => 'http://www.bbc.co.uk/radio2',
		)
	);
?>
 <div class='row'>
    <div class='col-md-offset-2 col-md-8'>
      <div class="carousel slide" data-ride="carousel" id="quote-carousel">
        <!-- Bottom Carousel Indicators -->
        <ol class="carousel-indicators">
        	<?php $x=0;
        	foreach (get_testimonials() as $testimonial):
        		if ($x==0) {
        			$class = " class=\"active\"";
        		} else {
        			$class = "";
        		} ?>
          
				<li data-target="#quote-carousel" data-slide-to="<?php echo $x; ?>"<?php echo $class; ?>></li>
          <?php 
          $x++;
          endforeach;
          ?>
        </ol>
        
        <!-- Carousel Slides / Quotes -->
        <div class="carousel-inner">
        
        	<?php $x=0;
        	foreach ($testimonials as $testimonial):
        	if ($x==0) {
	        	$class="item active";
        	} else {
	        	$class="item";
        	}
        	?>
          <!-- Quote <?php echo $x+1; ?> -->
          <div class="<?php echo $class; ?>">
            <blockquote>
              <div class="row">
                <div class="col-sm-3 text-center">
                  <img class="img-circle" src="<?php echo $testimonial['image']; ?>" style="width: 100px;height:100px;">
                </div>
                <div class="col-sm-9">
                  <p><?php echo $testimonial['quote']; ?></p>
                  <small><?php echo $testimonial['author']; ?></small>
                </div>
              </div>
            </blockquote>
          </div>
          <?php $x++; endforeach; ?>
          
      </div>                          
    </div>
  </div>